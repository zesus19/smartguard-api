package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"] = append(beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"],
		beego.ControllerComments{
			"SyncMeta",
			`/sync/meta`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"] = append(beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"],
		beego.ControllerComments{
			"Now",
			`/sync/time`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"] = append(beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"],
		beego.ControllerComments{
			"GetProfile",
			`/sync/profile`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"] = append(beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"],
		beego.ControllerComments{
			"GetUploadCredential",
			`/image/credential`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"] = append(beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"],
		beego.ControllerComments{
			"ListSchool",
			`/school`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"] = append(beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"],
		beego.ControllerComments{
			"AcceptSchool",
			`/school/connect`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"] = append(beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"],
		beego.ControllerComments{
			"ListConnected",
			`/school/connected`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"] = append(beego.GlobalControllerRouter["gitchina/smartguard-api/controllers:ApiController"],
		beego.ControllerComments{
			"Upload",
			`/upload`,
			[]string{"post"},
			nil})

}
