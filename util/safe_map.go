package util

import (
	"sync"
)

type SafeMap struct {
	items map[interface{}]interface{}
	sync.RWMutex
}

type KeyValuePair struct {
	Key   interface{}
	Value interface{}
}

// Creates a new safe map
func NewSafeMap() *SafeMap {
	return &SafeMap{items: make(map[interface{}]interface{})}
}

// Sets the given value under the specified key.
func (m *SafeMap) Set(key interface{}, value interface{}) {
	m.Lock()
	defer m.Unlock()
	m.items[key] = value
}
func (m *SafeMap) Remove(key interface{}) interface{} {
	if v, ok := m.Get(key); ok {
		m.Lock()
		defer m.Unlock()
		delete(m.items, key)
		return v
	}
	return nil
}

func (m *SafeMap) SetIfNotExist(key interface{}, value interface{}) (interface{}, bool) {
	m.Lock()
	defer m.Unlock()
	if val, ok := m.items[key]; ok {
		return val, false
	}
	m.items[key] = value
	return value, true
}

// Retrieves an element from map under given key.
func (m *SafeMap) Get(key interface{}) (interface{}, bool) {
	m.RLock()
	defer m.RUnlock()

	val, ok := m.items[key]
	return val, ok
}

func (m *SafeMap) IterAllKeys() []interface{} {
	m.RLock()
	defer m.RUnlock()
	keys := make([]interface{}, 0, 100)
	for k, _ := range m.items {
		keys = append(keys, k)
	}
	return keys
}

func (m *SafeMap) IterAllValues() []interface{} {
	m.RLock()
	defer m.RUnlock()
	values := make([]interface{}, 0, 100)
	for _, v := range m.items {
		values = append(values, v)
	}
	return values
}

func (m *SafeMap) IterAll() []KeyValuePair {
	m.RLock()
	defer m.RUnlock()
	kvpList := make([]KeyValuePair, 0, 100)
	for k, v := range m.items {
		kvpList = append(kvpList, KeyValuePair{Key: k, Value: v})
	}
	return kvpList
}
