package main

import (
	_ "fmt"
	_ "gitchina/smartguard-api/routers"
	"gitchina/smartguard-api/util"

	"github.com/astaxie/beego"
)

func main() {
	//runmode := os.Getenv("CZRUNMODE")
	//beego.BConfig.RunMode = runmode
	beego.LoadAppConfig("ini", "conf/app.conf")
	util.InitLogger(beego.BConfig.RunMode)
	//data.InitOrm(beego.BConfig.RunMode)

	if beego.BConfig.RunMode != "prod" {
		beego.BConfig.Log.AccessLogs = true
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"

	}

	adapter, config := util.GetLoggerProfile()
	beego.BConfig.Log.Outputs = map[string]string{adapter: config}
	beego.Run()
}
