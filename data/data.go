package data

import (
	"github.com/astaxie/beego/orm"
)

type School struct {
	ID         int    `orm:"pk;auto;column(id)"`
	Name       string `orm:"column(name)"`
	Data       string `orm:"column(data)"`
	Remark     string `orm:"column(remark)"`
	CreateTime string `orm:"column(create_time)"`
	UpdateTime string `orm:"column(update_time)"`
}

func init() {
	orm.RegisterModel(new(School))
}

func (s *School) TableName() string {
	return "school"
}

func AddOrUpdateSchool(s School) (err error) {
	var id int64
	var created bool
	o := orm.NewOrm()
	if created, id, err = o.ReadOrCreate(&s, "Name"); err == nil {
		if !created {
			s.ID = int(id)
			o.Update(&s, "Name", "Data", "Remark")
		}
	}
	return err
}

func GetSchoolList() (list []School, err error) {
	return nil, nil
}
