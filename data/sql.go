package data

import (
	"gitchina/smartguard-api/util"

	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

var connection string

func InitOrm(runmode string) {
	if runmode != "prod" {
		orm.Debug = true
		if runmode == "uat" {
			//connection = "prod_klydb:@WW%_)2a@tcp(rm-bp10j3s1681i41x20.mysql.rds.aliyuncs.com:3306)/KlyDB?charset=utf8"
		} else if runmode == "test" {
			//connection = "test:123456@tcp(120.26.223.135:22479)/KlyDB?charset=utf8"
		} else {
			connection = "root:123456@tcp(127.0.0.1:3306)/SmartGuardDB?charset=utf8"
		}
	}

	orm.DebugLog = orm.NewLog(util.UseBeeLogger())
	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", connection)
}
