package test

import (
	"fmt"
	"gitchina/smartguard-api/models"
	"gitchina/smartguard-api/tests/common"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func init() {
	common.InitCommon()
}

func TestAll(t *testing.T) {
	fmt.Println("====================================================================================================================================================================================")
	testSchoolAddOrUpdate(t)
}

func testSchoolAddOrUpdate(t *testing.T) {
	Convey("test school add or update", t, func() {
		s := models.School{Name: "a", Data: "b", Remark: "c"}
		ok := s.AddOrUpdate()
		So(ok, ShouldEqual, true)
	})
}
